from vm import Vm
import argparse

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Execute a Brainfuck program')
    parser.add_argument('filename', help='the file containing the program to execute')
    parser.add_argument('-d', '--debug', dest='debug', action='store_true', default=False, help='execute the program in debugging mode')
    parser.add_argument('-t', '--time', dest='timed', action='store_true', default=False, help='time the execution of the program')

    args = parser.parse_args()
    programfile = open(args.filename, 'r')
    program = programfile.read()

    vm = Vm(program)
    if args.debug:
        vm.debug()
    else:
        vm.run(args.timed)