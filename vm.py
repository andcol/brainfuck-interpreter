from time import sleep
from stopwatch import Stopwatch

class Vm:
    def __init__(self, program):
        self.program = program
        self.tape = [0]
        self.pc = 0
        self.cursor = 0
        self.jump_table = {}

    def step(self, stopwatch):
        if(self.program[self.pc] == '>'): 
            self.cursor += 1
            if self.cursor >= len(self.tape):
                self.tape += [0]

        elif(self.program[self.pc] == '<'):
            if self.cursor > 0:
                self.cursor -= 1

        elif(self.program[self.pc] == '+'):
            self.tape[self.cursor] += 1

        elif(self.program[self.pc] == '-'):
            self.tape[self.cursor] -= 1

        elif(self.program[self.pc] == '.'):
            print('> ' + str(self.tape[self.cursor]))

        elif(self.program[self.pc] == ','): 
            if stopwatch is not None:
                stopwatch.stop() #do not count time waiting for input
            self.tape[self.cursor] = int(input('< '))
            if stopwatch is not None:
                stopwatch.start()
            

        elif(self.program[self.pc] == '['):
            if(self.tape[self.cursor] == 0):
                if(self.pc not in self.jump_table):
                    starting_point = self.pc
                    nesting = 1
                    while not (self.program[self.pc] == ']' and nesting == 0) and self.pc < len(self.program)-1:
                        self.pc += 1
                        if(self.program[self.pc] == '['):
                            nesting += 1
                        elif(self.program[self.pc] == ']'):
                            nesting -= 1
                    self.jump_table[starting_point] = self.pc
                else:
                    self.pc = self.jump_table[self.pc]

        elif(self.program[self.pc] == ']'):
            if(self.tape[self.cursor] != 0):
                if(self.pc not in self.jump_table):
                    starting_point = self.pc
                    nesting = 1
                    while not (self.program[self.pc] == '[' and nesting == 0) and self.pc > 0:
                        self.pc -= 1
                        if(self.program[self.pc] == '['):
                            nesting -= 1
                        elif(self.program[self.pc] == ']'):
                            nesting += 1
                    self.jump_table[starting_point] = self.pc
                else:
                    self.pc = self.jump_table[self.pc]

        self.pc += 1

    def run(self, timed):
        if timed:
            stopwatch = Stopwatch()
            stopwatch.start()
            while(self.pc < len(self.program)):
                self.step(stopwatch)
            print('Execution took ' + str(stopwatch.get_time()*1000) + 'ms.')
        else:
            while(self.pc < len(self.program)):
                self.step()
            

    def printstate(self):
        #PRINT PROGRAM
        l1 = '┌'
        for _ in range(len(self.program)):
            l1 += '───┬'
        l1 = l1[:-1]
        l1 += '┐'
        print(l1)

        l2 = '│'
        for cmd in self.program:
          l2 += ' '+ cmd + ' │'
        print(l2)

        l3 = '└'
        for _ in range(len(self.program)):
            l3 += '───┴'
        l3 = l3[:-1]
        l3 += '┘'
        print(l3)

        l4 = ''
        for _ in range(self.pc):
            l4 += '    '
        l4 += '  ^  '
        print(l4)

        #PRINT TAPE
        l1 = '┌'
        for _ in range(len(self.tape)):
            l1 += '─────┬'
        l1 = l1[:-1]
        l1 += '┐'
        print(l1)

        l2 = '│'
        for datum in self.tape:
          l2 += ' '+ str(datum) + ''.join([' ' for _ in range(3-len(str(datum)))]) + ' │'
        print(l2)

        l3 = '└'
        for _ in range(len(self.tape)):
            l3 += '─────┴'
        l3 = l3[:-1]
        l3 += '┘'
        print(l3)

        l4 = ''
        for _ in range(self.cursor):
            l4 += '      '
        l4 += '   ^   '
        print(l4)

    def debug(self):
        auto = False
        while(self.pc < len(self.program)):
            self.printstate()

            if auto:
                sleep(autodelay)
                cmd = 'step 1'
            else:
                cmd = input('$ ')

            action = cmd.split(' ')

            if(action[0] == 'step'):
                for _ in range(int(action[1]) if len(action) > 1 else 1):
                    self.step()
            elif(action[0] == 'auto'):
                auto = True
                autodelay = int(action[1])**(-1) if len(action) > 1 else 1
