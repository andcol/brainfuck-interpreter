# Brainfuck Interpreter

A simple interpreter for the Brainfuck esolang written in Python, equipped with an even simpler interactive debugger. To execute a program run

```
python3 bf.py <filename>
```

A request for an integer input is marked by `<`, while the program's output is marked by `>`.

To debug a program, run

```
python3 bf.py <filename> -d
```

The interactive debugger currently supports the following commands:

- `step [n]` performs `n` steps of the program execution (default `n = 1`).
- `auto [n]` sets the interpreter to automatically step the program execution `n` times per second (default `n = 1`).

To time the execution of a program, run

```
python3 bf.py <filename> -t
```