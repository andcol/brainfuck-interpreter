from time import process_time

class Stopwatch:
    def __init__(self):
        self.time = 0
        self.last_start = 0

    def start(self):
        self.last_start = process_time()

    def stop(self):
        self.time += process_time() - self.last_start

    def reset(self):
        self.time = 0
        self.last_start = 0

    def get_time(self):
        return self.time + (process_time() - self.last_start)